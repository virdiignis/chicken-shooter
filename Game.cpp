//
// Created by prance on 17.05.18.
//

#include "Game.hpp"

Game::Game() {
    window->setFramerateLimit(60);
    background_texture.loadFromFile(BACKGROUND_FILE_PATH);
    background.setTexture(background_texture, true);
    generator.seed(std::random_device()());
    shootBuff.loadFromFile(SHOOT_SOUND_FILE_PATH);
    shoot.setBuffer(shootBuff);
    backBuff.loadFromFile(BACKSOUND_FILE_PATH);
    backsound.setBuffer(backBuff);
    backsound.setLoop(true);
    hits.setText("Score: ");
    hits.reset();
    rounds.setText("Ammo: ");
    rounds += 5;
    rounds.setPosition(window->getSize().x - 350, window->getSize().y - 100);
    hits.setPosition(50, window->getSize().y - 100);
}

void Game::renderGameObjects() {
    window->draw(background);
    std::list<Chicken *> temporary_list_of_chickens_to_remove; //That's hilarious, ok?
    for (auto &chicken: chickens) if (!chicken->isVisible()) temporary_list_of_chickens_to_remove.push_back(chicken);
    for (auto &chicken: temporary_list_of_chickens_to_remove) chickens.remove(chicken);
    for (auto &chicken: chickens) window->draw(*chicken);
    window->draw(hits);
    window->draw(rounds);
}

void Game::addChicken() {
    auto chicken = new Chicken(window);
    short int side = distributionStart(generator);
    chicken->setPosition(side * window->getSize().x, distributionStartY(generator));
    chicken->setV((side ? -1 : 1) * distributionVelocityX(generator), distributionVelocityY(generator));
    chicken->setScale(side ? 1 : -1, 1);
    chickens.push_back(chicken);
}

void Game::moveChickens() {
    for (auto &chicken: chickens) chicken->move();
    std::list<Chicken *> temporary_list_of_chickens_to_remove; //That's hilarious, ok?
    auto size = window->getSize();
    for (auto &chicken: chickens) {
        auto position = chicken->getPosition();
        if (size.x + 80 < position.x || size.y + 80 < position.y || position.x < -80 || position.y < -80)
            temporary_list_of_chickens_to_remove.push_back(chicken);
    }
    for (auto &chicken: temporary_list_of_chickens_to_remove) chickens.remove(chicken);
}

void Game::eventLoop() {
    sf::Event Event{};
    backsound.play();
    while (window->isOpen()) {
        switch (gameState) {
            case 0:
                menu.show();
                break;
            case 1:
                window->clear();
                balanceChickens();
                moveChickens();
                renderGameObjects();
                break;
            case 2:
                endGameScreen.setScore(hits, shots);
                endGameScreen.show();
                break;
            default:
                throw std::runtime_error("Gamestate is invalid, something went terribly wrong.");
        }
        while (window->pollEvent(Event)) {
            switch (Event.type) {
                case sf::Event::Closed:
                    window->close();
                    break;
                case sf::Event::KeyPressed:
                    switch (Event.key.code) {
                        case sf::Keyboard::A:
                            addChicken();
                            break;
                        case sf::Keyboard::M:
                            gameState = !gameState;
                            break;
                        case sf::Keyboard::O:
                            moveChickens();
                            break;
                        case sf::Keyboard::Q:
                            window->close();
                            break;
                    }
                    break;
                case sf::Event::MouseButtonPressed:
                    switch (gameState) {
                        case 0:
                            menu.checkClicks();
                            if (menu.getAction()) handleMenuAction();
                            break;
                        case 1:
                            shoot.play();
                            rounds -= 1;
                            shots++;
                            checkShoots();
                            checkRounds();
                            break;
                        case 2:
                            if (endGameScreen.checkClicks()) {
                                gameState = 0;
                                newGame();
                            }
                            break;
                    }
                    break;
            }
        }
        window->display();
    }
}

void Game::checkShoots() {
    for (auto &chicken: chickens) {
        hits += chicken->checkClick();
        rounds += chicken->checkClick();
    }
}

void Game::handleMenuAction() {
    switch (menu.getAction()) {
        case 2:
            newGame();
            break;
        case 3:
            window->close();
            break;
    }
    menu.actionHandle();
    gameState = 1;
}

void Game::newGame() {
    chickens.clear();
    shots = 0;
    hits.reset();
    rounds.reset();
    rounds += CHICKENS_NO;
    for (unsigned short i = 0; i < CHICKENS_NO; ++i) addChicken();
}

void Game::balanceChickens() {
    for (int i = 0; i < CHICKENS_NO - chickens.size(); ++i) addChicken();
}

void Game::checkRounds() {
    if (rounds == 0) endGame();
}

void Game::endGame() {
    gameState = 2;
}
