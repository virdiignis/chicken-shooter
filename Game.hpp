//
// Created by prance on 17.05.18.
//

#ifndef CHICKEN_SHOOTER_GAME_H
#define CHICKEN_SHOOTER_GAME_H

#define BACKGROUND_FILE_PATH "../background.jpg"
#define SHOOT_SOUND_FILE_PATH "../shoot.wav"
#define BACKSOUND_FILE_PATH "../backsounds.wav"
#define CHICKENS_NO 5

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Audio.hpp>
#include "Menu.hpp"
#include "Chicken.hpp"
#include "Counter.hpp"
#include "EndGameScreen.hpp"
#include <list>
#include <random>


class Game {
private:
    sf::RenderWindow *window = new sf::RenderWindow(sf::VideoMode(1199, 658), "Chicken shooter");
    std::default_random_engine generator;
    std::uniform_real_distribution<float> distributionVelocityX = std::uniform_real_distribution<float>(4.0, 12.0);
    std::uniform_real_distribution<float> distributionVelocityY = std::uniform_real_distribution<float>(
            static_cast<float>(-1.0), 1.0);
    std::uniform_real_distribution<float> distributionStartY = std::uniform_real_distribution<float>(50.0,
                                                                                                     window->getSize().y -
                                                                                                     50);
    std::uniform_int_distribution<short int> distributionStart = std::uniform_int_distribution<short int>(0, 1);
    unsigned short gameState = 0, shots = 0;
    sf::Texture background_texture;
    sf::Sprite background;
    Menu menu = Menu(window);
    std::list<Chicken *> chickens;
    sf::SoundBuffer shootBuff, backBuff;
    sf::Sound shoot, backsound;

    EndGameScreen endGameScreen = EndGameScreen(window);
    Counter hits, rounds;

    void balanceChickens();

    void renderGameObjects();

    void addChicken();

    void moveChickens();

    void checkShoots();

    void checkRounds();

    void handleMenuAction();

    void newGame();

    void endGame();

public:
    Game();

    void eventLoop();
};


#endif //CHICKEN_SHOOTER_GAME_H
