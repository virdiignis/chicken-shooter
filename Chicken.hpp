//
// Created by prance on 17.05.18.
//

#ifndef CHICKEN_SHOOTER_CHICKEN_H
#define CHICKEN_SHOOTER_CHICKEN_H
#define CHICKEN_PNG "../chicken.png"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Mouse.hpp>


class Chicken : public sf::Sprite {
private:
    sf::Texture texture;
    sf::RenderWindow *window;
    float v_x, v_y;
    bool visible = true;
    int no = rand();
public:
    bool isVisible() const;

    explicit Chicken(sf::RenderWindow *&);

    void setV(float, float);

    void move();

    bool checkClick();

    bool operator==(const Chicken &rhs) const;

    bool operator!=(const Chicken &rhs) const;

};


#endif //CHICKEN_SHOOTER_CHICKEN_H
