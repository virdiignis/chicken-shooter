//
// Created by prance on 16.05.18.
//

#ifndef CHICKEN_SHOOTER_MENU_H
#define CHICKEN_SHOOTER_MENU_H

#define FONT_FILEPATH "../font.ttf"
#define MENU_TEXTS 3

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Mouse.hpp>
#include <array>

class Menu {
public:
    explicit Menu(sf::RenderWindow *&);

    void show();

    void checkClicks();

    unsigned short getAction() const;

    void actionHandle();

    sf::RenderWindow *window;

private:
    unsigned short action = 0;


    sf::Font font;
    std::array<sf::Text, MENU_TEXTS> texts;
};


#endif //CHICKEN_SHOOTER_MENU_H
