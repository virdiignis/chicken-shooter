//
// Created by prance on 16.05.18.
//

#include "Menu.hpp"


Menu::Menu(sf::RenderWindow *&w) {
    window = w;
    font.loadFromFile(FONT_FILEPATH);
    std::array<std::string, MENU_TEXTS> options = {"Menu", "New Game", "Close game :("};
    unsigned short int i = 0;
    for (auto &text : texts) {
        text.setFont(font);
        text.setFillColor(sf::Color::Green);
        text.setCharacterSize(68);
        text.setPosition(static_cast<float>((window->getSize().x) / 3.0),
                         static_cast<float>((window->getSize().y) / 5.0 + i * 80));
        text.setString(options[i]);
        i++;
    }
}

void Menu::show() {
    window->clear();
    for (auto &text : texts) window->draw(text);
}

void Menu::checkClicks() {
    unsigned short int i = 1;
    for (auto &text: texts) {
        if (text.getGlobalBounds().contains(sf::Mouse::getPosition(*window).x, sf::Mouse::getPosition(*window).y))
            break;
        i++;
    }
    action = i;
}

unsigned short Menu::getAction() const {
    return action;
}

void Menu::actionHandle() {
    action = 0;
}
