//
// Created by prance on 24.05.18.
//

#ifndef CHICKEN_SHOOTER_ENDGAMESCREEN_HPP
#define CHICKEN_SHOOTER_ENDGAMESCREEN_HPP

#define FONT_FILEPATH "../font.ttf"
#define END_SCREEN_TEXTS 5

#include "Menu.hpp"
#include "Counter.hpp"

class EndGameScreen {
private:
    sf::RenderWindow *window;
    sf::Font font;
    std::array<sf::Text, END_SCREEN_TEXTS> texts;
public:
    explicit EndGameScreen(sf::RenderWindow *&);

    void show() const;

    bool checkClicks() const;

    void setScore(Counter, unsigned short);
};


#endif //CHICKEN_SHOOTER_ENDGAMESCREEN_HPP
