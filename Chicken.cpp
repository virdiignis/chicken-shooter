//
// Created by prance on 17.05.18.
//

#include "Chicken.hpp"

Chicken::Chicken(sf::RenderWindow *&w) {
    visible = true;
    window = w;
    texture.loadFromFile(CHICKEN_PNG);
    this->setTexture(texture);
    srand(static_cast<unsigned int>(time(nullptr)));
}

void Chicken::move() {
    this->setPosition(this->getPosition() + sf::Vector2f(v_x, v_y));
}

bool Chicken::checkClick() {
    if (this->getGlobalBounds().contains(sf::Mouse::getPosition(*window).x, sf::Mouse::getPosition(*window).y))
        visible = false;
    return !visible;
}

void Chicken::setV(float v_x, float v_y) {
    Chicken::v_x = v_x;
    Chicken::v_y = v_y;
}

bool Chicken::isVisible() const {
    return visible;
}

bool Chicken::operator==(const Chicken &rhs) const {
    return no == rhs.no;
}

bool Chicken::operator!=(const Chicken &rhs) const {
    return !(rhs == *this);
}
