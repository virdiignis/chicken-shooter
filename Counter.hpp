//
// Created by prance on 23.05.18.
//

#ifndef CHICKEN_SHOOTER_HITSCOUNTER_HPP
#define CHICKEN_SHOOTER_HITSCOUNTER_HPP

#define FONT_FILEPATH "../font.ttf"

#include <SFML/Graphics/Text.hpp>

class Counter : public sf::Text {
private:
    unsigned short number = 0;
    sf::Font font;
    std::string text;

    void render();

public:
    Counter();

    unsigned short getNumber() const;

    void setText(std::string);

    void reset();

    void operator+=(int);

    void operator-=(int);

    bool operator==(int) const;
};


#endif //CHICKEN_SHOOTER_HITSCOUNTER_HPP
