//
// Created by prance on 23.05.18.
//

#include "Counter.hpp"

Counter::Counter() {
    font.loadFromFile(FONT_FILEPATH);
    setFont(font);
    setFillColor(sf::Color::Red);
    setCharacterSize(72);
    setPosition(10, 0);
}

void Counter::operator+=(int h) {
    number += h;
    render();
}

void Counter::operator-=(int h) {
    number -= h;
    render();
}

void Counter::render() {
    setString(text + std::to_string(number));
}

bool Counter::operator==(int h) const {
    return number == h;
}

void Counter::reset() {
    number = 0;
    render();
}

void Counter::setText(std::string text) {
    Counter::text = std::move(text);
}

unsigned short Counter::getNumber() const {
    return number;
}
