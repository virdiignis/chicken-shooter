//
// Created by prance on 24.05.18.
//

#include <fstream>
#include "EndGameScreen.hpp"

EndGameScreen::EndGameScreen(sf::RenderWindow *&w) {
    window = w;
    font.loadFromFile(FONT_FILEPATH);
    std::array<std::string, END_SCREEN_TEXTS> options = {"Game Over", "", "", "", "Menu"};
    unsigned short int i = 0;
    for (auto &text : texts) {
        text.setFont(font);
        text.setFillColor(sf::Color::Red);
        text.setCharacterSize(68);
        text.setPosition(static_cast<float>((window->getSize().x) / 4.0),
                         static_cast<float>((window->getSize().y) / 8.0 + i * 80));
        text.setString(options[i]);
        i++;
    }
}

void EndGameScreen::show() const {
    window->clear();
    for (auto &text : texts) window->draw(text);
}

bool EndGameScreen::checkClicks() const {
    return texts[4].getGlobalBounds().contains(sf::Mouse::getPosition(*window).x, sf::Mouse::getPosition(*window).y);
}

void EndGameScreen::setScore(Counter counter, unsigned short shots) {
    texts[1].setString(counter.getString());
    std::ifstream si_bestScore("../.bestscore");
    unsigned short best;
    si_bestScore >> best;
    best = std::max(best, counter.getNumber());
    si_bestScore.close();
    std::ofstream so_bestScore("../.bestscore");
    so_bestScore << best;
    texts[2].setString("Best score: " + std::to_string(best));
    so_bestScore.close();
    float accuracy = static_cast<float>(counter.getNumber() * 100) / shots;
    texts[3].setString("Accuracy: " + std::to_string(accuracy) + "%");
    std::ofstream so_Stats("../.stats", std::fstream::app);
    so_Stats << counter.getNumber() << "," << shots << std::endl;
    so_Stats.close();
}
